<?php 
if(!defined('BASEPATH'))exit('No direct script access allowed');
define('DB_HOST', 'localhost');
define('DB_USER', 'erp_user'); 
define('DB_PWD', 'erp_pass'); 
define('DB_NAME', 'erp'); 
/* End of file constant.php */
/* Location: ./shared/config/constant.php */
