<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Goods extends CI_Controller {

    public function __construct(){
        parent::__construct();
		$this->purview_model->checkpurview(68);
		$this->load->model('data_model');
    }
	
	public function index(){
		$this->load->view('goods/index');
	}

	//商品添加修改
	public function save() {
	    $id  = intval($this->input->post('id',TRUE));
		$act = str_enhtml($this->input->get('act',TRUE));
		$info['categoryId'] = $data['categoryid'] = intval($this->input->post('categoryId',TRUE));
		$info['unitId']     = $data['unitid'] = intval($this->input->post('baseUnitId',TRUE));
		$info['name']       = $data['name']   = str_enhtml($this->input->post('name',TRUE));
		$info['number']     = $data['number'] = str_enhtml($this->input->post('number',TRUE));
		$info['purPrice']   = $data['purprice'] = (float)$this->input->post('purPrice',TRUE);
		$info['remark']     = $data['remark'] = str_enhtml($this->input->post('remark',TRUE));
		$info['salePrice']  = $data['saleprice'] = (float)$this->input->post('salePrice',TRUE);
		$info['spec']       = $data['spec'] = str_enhtml($this->input->post('spec',TRUE));
		$info['unitCost']   = $data['unitcost'] = (float)$this->input->post('unitcost',TRUE);
		$info['quantity']   = $data['quantity'] = (float)$this->input->post('quantity',TRUE);
		$info['amount']     = $data['amount'] = (float)$this->input->post('amount',TRUE);
		$data['goods']      = $info['number'].' '.$info['name'].'_'.$data['spec'];
		
		strlen($data['name']) < 1 && die('{"status":-1,"msg":"名称不能为空"}'); 
		$data['categoryid'] < 1   && die('{"status":-1,"msg":"请选择商品分类"}'); 
		$data['unitid'] < 1       && die('{"status":-1,"msg":"请选择单位"}'); 
		$info['categoryName']   = $data['categoryname'] = $this->mysql_model->db_one(CATEGORY,'(id='.$data['categoryid'].')','name');
		$info['unitName']   = $data['unitname']     = $this->mysql_model->db_one(UNIT,'(id='.$data['unitid'].')','name');
		!$data['categoryname'] || !$data['unitname']  && die('{"status":-1,"msg":"参数错误"}');
		
		if ($act=='add') {
		    $this->purview_model->checkpurview(69);
			$this->mysql_model->db_count(GOODS,'(number="'.$data['number'].'")') > 0 && die('{"status":-1,"msg":"商品编号重复"}');
		    $sql = $this->mysql_model->db_inst(GOODS,$data);
			if ($sql) {
			    $info['id'] = $sql;
				$this->cache_model->delsome(GOODS);
				$this->data_model->logs('新增商品:'.$data['name']);
				die('{"status":200,"msg":"success","data":'.json_encode($info).'}');
			} else {
			    die('{"status":-1,"msg":"添加失败"}');
			}
		} elseif ($act=='update') {
		    $this->purview_model->checkpurview(70);
			$this->mysql_model->db_count(GOODS,'(id<>'.$id.') and (number="'.$data['number'].'")') > 0 && die('{"status":-1,"msg":"商品编号重复"}');
			$name = $this->mysql_model->db_one(GOODS,'(id='.$id.')','name');
		    $sql = $this->mysql_model->db_upd(GOODS,$data,'(id='.$id.')');
			if ($sql) {
			    $info['id'] = $id;
				$info['propertys'] = array();
			    $this->cache_model->delsome(GOODS);
				$this->data_model->logs('修改商品:'.$name.' 修改为 '.$data['name']);
				die('{"status":200,"msg":"success","data":'.json_encode($info).'}');
			} else {
				die('{"status":-1,"msg":"修改失败"}');
			}
		}
	}

	//商品删除
    public function del() {
	    $this->purview_model->checkpurview(71);
	    $id = str_enhtml($this->input->post('id',TRUE));
		if (strlen($id) > 0) {
		    $this->mysql_model->db_count(INVPU_INFO,'(goodsid in('.$id.'))')>0 && die('{"status":-1,"msg":"其中有商品发生业务不可删除"}'); 
			$this->mysql_model->db_count(INVSA_INFO,'(goodsid in('.$id.'))')>0 && die('{"status":-1,"msg":"其中有商品发生业务不可删除"}'); 
			$this->mysql_model->db_count(INVOI_INFO,'(goodsid in('.$id.'))')>0 && die('{"status":-1,"msg":"其中有商品发生业务不可删除"}'); 
		    $sql = $this->mysql_model->db_del(GOODS,'(id in('.$id.'))');   
		    if ($sql) {
			    $this->cache_model->delsome(GOODS);
				$this->data_model->logs('删除商品:ID='.$id);
				die('{"status":200,"msg":"success","data":{"msg":"","id":['.$id.']}}');
			} else {
			    die('{"status":-1,"msg":"删除失败"}');
			}
		}
	}
	
	//商品导出
	public function export() {
	    $this->purview_model->checkpurview(72);
	    sys_xls('商品明细.xls');
		$skey         = str_enhtml($this->input->get('skey',TRUE));
		$categoryid   = intval($this->input->get('assistId',TRUE));
		$where = '';
		if ($skey) {
			$where .= ' and goods like "%'.$skey.'%"';
		}
		if ($categoryid > 0) {
		    $cid = $this->cache_model->load_data(CATEGORY,'(1=1) and find_in_set('.$categoryid.',path)','id'); 
			if (count($cid)>0) {
			    $cid = join(',',$cid);
			    $where .= ' and categoryid in('.$cid.')';
			} 
		}
		$this->data_model->logs('导出商品');
		$data['list'] = $this->cache_model->load_data(GOODS,'(status=1) '.$where.' order by id desc');  
		$this->load->view('goods/export',$data);
	}	

	public function import() {
		if (!empty($_FILES)) {
			@require_once './system/libraries/Excel/reader.php';
			$file = $_FILES['file']['name'];
			$filetempname = $_FILES['file']['tmp_name'];
			//注意设置时区
			$time = date("y-m-d-H-i-s"); //去当前上传的时间
			//获取上传文件的扩展名
			$extend = strrchr($file, '.');
			//上传后的文件名
			$name = $time . $extend;
			$uploadfile = FCPATH."/data/uploads/" . $name; //上传后的文件名地址
			//move_uploaded_file() 函数将上传的文件移动到新位置。若成功，则返回 true，否则返回 false。
			$result = move_uploaded_file($filetempname,$uploadfile); //假如上传到当前目录下
				

			if ($result) //如果上传文件成功，就执行导入excel操作
			{
			
				$xls = new Spreadsheet_Excel_Reader();
					$xls->setOutputEncoding('utf-8');
					$xls->read($uploadfile);
					$i=4;
					$len = $xls->sheets[0]['numRows'];
			
					while($i<=$len){
						$temp = $xls->sheets[0]['cells'][$i][2];
						if (!empty($temp)) {
							$log = $this->mysql_model->db_one(GOODS,"(number='".$temp."')",'id');
							if (empty($log)) {//没有记录 插入
								$insert = array(
									'name' => str_enhtml($xls->sheets[0]['cells'][$i][3]),
									'number' => $xls->sheets[0]['cells'][$i][2],
									'quantity' => (float)$xls->sheets[0]['cells'][$i][10],
									'spec' => @str_enhtml($xls->sheets[0]['cells'][$i][4]),
									'unitid' => $this->getunitIdByname($xls->sheets[0]['cells'][$i][6]),
									'unitname' => $xls->sheets[0]['cells'][$i][6],
									'categoryid' => $this->getcategoryIdByname($xls->sheets[0]['cells'][$i][5]),
									'categoryname' => $xls->sheets[0]['cells'][$i][5],
									'purprice' => (float)$xls->sheets[0]['cells'][$i][7],
									'saleprice' => (float)$xls->sheets[0]['cells'][$i][8],
									'unitcost' => (float)$xls->sheets[0]['cells'][$i][11],
									'amount' => (float)$xls->sheets[0]['cells'][$i][12],
									'remark' => @str_enhtml($xls->sheets[0]['cells'][$i][9]),
									);
								$insert['goods'] = $insert['number'].' '.$insert['name'].'_'.$insert['spec'];
								$this->mysql_model->db_inst(GOODS,$insert);
							}else{//有记录 更新
								$insert = array(
									'name' => str_enhtml($xls->sheets[0]['cells'][$i][3]),
									'number' => $xls->sheets[0]['cells'][$i][2],
									'quantity' => (float)$xls->sheets[0]['cells'][$i][10],
									'spec' => @str_enhtml($xls->sheets[0]['cells'][$i][4]),
									'unitid' => $this->getunitIdByname($xls->sheets[0]['cells'][$i][6]),
									'unitname' => $xls->sheets[0]['cells'][$i][6],
									'categoryid' => $this->getcategoryIdByname($xls->sheets[0]['cells'][$i][5]),
									'categoryname' => $xls->sheets[0]['cells'][$i][5],
									'purprice' => (float)$xls->sheets[0]['cells'][$i][7],
									'saleprice' => (float)$xls->sheets[0]['cells'][$i][8],
									'unitcost' => (float)$xls->sheets[0]['cells'][$i][11],
									'amount' => (float)$xls->sheets[0]['cells'][$i][12],
									'remark' => @str_enhtml($xls->sheets[0]['cells'][$i][9]),
									);
								$insert['goods'] = $insert['number'].' '.$insert['name'].'_'.$insert['spec'];
								$this->mysql_model->db_upd(GOODS,$insert,'(id='.$log.')');
							}
						}
						$i++;
						unset($temp);
						unset($insert);
						unset($log);
					}
					$this->data_model->logs('导入商品');
					$this->cache_model->clean();
					die('导入成功');
			} else {
				die('导入失败');
			}

		}
		$this->load->view('goods/import');
	}
	public function getunitIdByname($name){
		if (empty($name)) {
			$unitId = '';
		}else{
			$unitId = $this->mysql_model->db_one(UNIT,"(name='".$name."')",'id');
			if (empty($unitId)) {
				$insert = array(
					'name' => $name,
					'status' => 1,
					);
				$unitId = $this->mysql_model->db_inst(UNIT,$insert);
			}
		}
		return $unitId;
	}

	public function getcategoryIdByname($name){
		if (empty($name)) {
			$categoryId = '';
		}else{
			$categoryId = $this->mysql_model->db_one(CATEGORY,"(name='".$name."')",'id');
			if (empty($categoryId)) {
				$insert = array(
					'name' => $name,
					'pid' => 0,
					'depth' => 1,
					'ordnum' => 0,
					'status' => 1,
					'type' => 'trade',
					);
				$categoryId = $this->mysql_model->db_inst(CATEGORY,$insert);
				$data['path'] = $categoryId;
				$sql = $this->mysql_model->db_upd(CATEGORY,$data,'(id='.$categoryId.')');
			}
		}
		return $categoryId;
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */